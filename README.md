# Apotek UAS

This Application use CodeIgniter 4.

1. [Installation](#1-installation)
2. [Configuration](#2-configuration)
3. [Usage](#3-usage)

## 1. Installation

You can download it or Clone the repository
```bash
# with git cli
git clone https://gitlab.com/gitlab_username/apotek-uas.git
# or you can clone from git desktop / git gui
```
You can copy it to your www path or htdocs
```bash
# open the command prompt & open your project location
cd /%PATH%/apotek-uas

# install vendor with composer
cp env .env
```

## 2. Configuration

### 1. Configure ***.env***

Change File from
```bash
# CI_ENVIRONMENT = production
...
# database.default.hostname = localhost
# database.default.database = ci4
# database.default.username = root
# database.default.password = root
# database.default.DBDriver = MySQLi

```
into
```bash
# if you want to see the debug, you must set CI_ENVIRONMENT to development. 
# if you want to publish this, just let it be
CI_ENVIRONMENT = development
...
database.default.hostname = 127.0.0.1
database.default.database = yourdatabasename
database.default.username = root
database.default.password = yourpassword
database.default.DBDriver = MySQLi
```

### 2. Migrate Database

```bash
# migrate database
php spark migrate
# if table is exists you drop & remigrate
php spark migrate:refresh
```

## 3. Usage

```bash
# serve the application
php spark serve
```
You can access [http://localhost:8080](http://localhost:8080) or [http://localhost/apotek-uas/public](http://localhost/apotek-uas/public) to the application.