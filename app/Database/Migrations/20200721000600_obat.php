<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Obat extends Migration
{
    public function up()
    {
        $this->db->enableForeignKeyChecks();

        $this->forge->addField([
            'kode_obat'          => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'kode_supplier'          => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
            ],
            'nama_obat'       => [
                'type'           => 'VARCHAR',
                'constraint'     => 100,
            ],
            'produsen'       => [
                'type'           => 'VARCHAR',
                'constraint'     => 100,
                'null'           => true,
            ],
            'harga'       => [
                'type'           => 'INT',
                'constraint'     => 11,
                'null'           => true,
            ],
            'stok'       => [
                'type'           => 'INT',
                'constraint'     => 11,
            ],
            'foto'       => [
                'type'           => 'VARCHAR',
                'constraint'     => 255,
                'null'           => true,
            ],
        ]);
        $this->forge->addKey('kode_obat', true);
        $this->forge->addForeignKey('kode_supplier','supplier','kode_supplier');
        $this->forge->createTable('obat');
    }

    public function down()
    {
        $this->forge->dropTable('obat');
    }
}