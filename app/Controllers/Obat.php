<?php namespace App\Controllers;

use App\Models\ObatModel;
use App\Models\SupplierModel;

class Obat extends BaseController
{
    public function __construct()
    {
        $this->obat = new ObatModel();
        $this->supplier = new SupplierModel();
    }

	public function index()
	{
        $data['web_title'] = 'Obat';
        
        $data['obat'] = $this->obat->getObat();
		return view('obat/home_page', $data);
    }
    
    public function tambah()
    {
        $data['web_title'] = 'Obat';

        $data['supplier'] = $this->supplier->getSupplier();
        return view('obat/form_page', $data);
    }

    public function edit($id)
    {
        $data['web_title'] = 'Obat';
        
        $data['obat'] = $this->obat->getObat($id);
        $data['supplier'] = $this->supplier->getSupplier();
        return view('obat/form_page', $data);
    }

    //--------------------------------------------------------------------
    
    // Function CRUD

    public function create()
    {
        // Membuat Array Collection
        $data = [
            'kode_obat' => $this->request->getPost('kode_obat'),
            'kode_supplier' => $this->request->getPost('kode_supplier'),
            'nama_obat' => $this->request->getPost('nama_obat'),
            'produsen' => $this->request->getPost('produsen'),
            'harga' => $this->request->getPost('harga'),
            'stok' => $this->request->getPost('stok'),
            'foto' => $this->request->getPost('foto'),
        ];

        $file = $this->request->getFile('foto');

        if($file->isValid())
        {
            $newName = $file->getRandomName();
            $file->move(ROOTPATH.'public/uploads', $newName);
            
            $data['foto'] = $file->getName();
        }

        $insert = $this->obat->saveObat($data);

        if($insert)
        {
            session()->setFlashdata('success', 'Berhasil Menambahkan Data Obat!');
            
            return redirect()->to(base_url('obat')); 
        }
    }

    public function update($id)
    {
        // Membuat Array Collection
        $data = [
            'kode_obat' => $this->request->getPost('kode_obat'),
            'kode_supplier' => $this->request->getPost('kode_supplier'),
            'nama_obat' => $this->request->getPost('nama_obat'),
            'produsen' => $this->request->getPost('produsen'),
            'harga' => $this->request->getPost('harga'),
            'stok' => $this->request->getPost('stok'),
            'foto' => $this->request->getPost('foto'),
        ];

        $file = $this->request->getFile('foto');

        if($file->isValid())
        {
            if(unlink(ROOTPATH.'public/uploads/'.$this->obat->getObat($id)['foto']))
            {
                $newName = $file->getRandomName();
                $file->move(ROOTPATH.'public/uploads', $newName);
    
                $data['foto'] = $file->getName();
            }else{
                session()->setFlashdata('danger', 'Gagal Menghapus Foto Sebelumnya!');
            
                return redirect()->to(base_url('obat')); 
            }
        }else{
            unset($data['foto']);
        }

        $insert = $this->obat->updateObat($data, $id);

        if($insert)
        {
            session()->setFlashdata('info', 'Berhasil Mengubah Data Obat!');
            
            return redirect()->to(base_url('obat')); 
        }
    }

    public function delete($id)
    {
        $delete = $this->obat->deleteObat($id);

        if($delete)
        {
            session()->setFlashdata('warning', 'Berhasil Menghapus Data Obat!');
            
            return redirect()->to(base_url('obat'));
        }
    }
}
