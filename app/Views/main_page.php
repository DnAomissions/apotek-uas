<?= $this->extend('main_layout') ?>

<?= $this->section('navbar') ?>
    <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="nav-link" href="<?=base_url()?>/">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>/obat">Obat</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>/supplier">Supplier</a>
        </li>
    </ul>
<?= $this->endSection() ?>

<?= $this->section('breadcrumb') ?>
    <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
    </ol>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
    <div class="jumbotron">
        <h1 class="display-4">Selamat Datang!</h1>
        <p class="lead">Ini adalah aplikasi untuk memenuhi pengerjaan Ujian Akhir Semester.</p>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Halaman Obat</h5>
                    <p class="card-text">Klik untuk pindah ke halaman Obat.</p>
                    <a href="<?=base_url()?>/obat" class="btn btn-primary">Obat</a>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Halaman Supplier</h5>
                    <p class="card-text">Klik untuk pindah ke halaman Supplier.</p>
                    <a href="<?=base_url()?>/supplier" class="btn btn-primary">Supplier</a>
                </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>