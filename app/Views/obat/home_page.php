<?= $this->extend('main_layout') ?>

<?= $this->section('navbar') ?>
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>/">Home</a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="<?=base_url()?>/obat">Obat <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>/supplier">Supplier</a>
        </li>
    </ul>
<?= $this->endSection() ?>

<?= $this->section('breadcrumb') ?>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=base_url()?>/">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Data Obat</li>
    </ol>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
    <div class="card">
        <div class="card-header">
            <a href="<?=base_url()?>/obat/tambah" class="btn btn-primary">Tambah</a>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead class="thead-light">
                    <tr>
                        <th>Kode Obat</th>
                        <th>Kode Supplier</th>
                        <th>Nama Obat</th>
                        <th>Produsen</th>
                        <th>Harga</th>
                        <th>Jumlah Stok</th>
                        <th>Foto</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    if(count($obat) <= 0) {
                ?>
                    <tr>
                        <td colspan="8" class="text-center">Data Kosong</td>
                    </tr>
                <?php
                    }
                    foreach ($obat as $key => $data) {
                ?>
                    <tr>
                        <td><?=$data['kode_obat']?></td>
                        <td><?=$data['kode_supplier']?></td>
                        <td><?=$data['nama_obat']?></td>
                        <td><?=$data['produsen']?></td>
                        <td><?=$data['harga']?></td>
                        <td><?=$data['stok']?></td>
                        <td><img src="uploads/<?=$data['foto']?>" alt="X" style="height: 5rem;"/></td>
                        <td>
                            <div class="btn-group">
                                <a href="<?=base_url()?>/obat/edit/<?=$data['kode_obat']?>" class="btn btn-sm btn-info">Ubah</a>
                                <a href="<?=base_url()?>/obat/delete/<?=$data['kode_obat']?>" onclick="return confirm('Apakah Anda yakin ingin menghapus <?= $data['nama_obat']; ?> ?')" class="btn btn-sm btn-danger">Hapus</a>
                            </div>
                        </td>
                    </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
<?= $this->endSection() ?>