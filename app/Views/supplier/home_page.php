<?= $this->extend('main_layout') ?>

<?= $this->section('navbar') ?>
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>/">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>/obat">Obat</a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="<?=base_url()?>/supplier">Supplier <span class="sr-only">(current)</span></a>
        </li>
    </ul>
<?= $this->endSection() ?>

<?= $this->section('breadcrumb') ?>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=base_url()?>/">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Data Supplier</li>
    </ol>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
    <div class="card">
        <div class="card-header">
            <a href="<?=base_url()?>/supplier/tambah" class="btn btn-primary">Tambah</a>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead class="thead-light">
                    <tr>
                        <th>Kode Supplier</th>
                        <th>Nama Supplier</th>
                        <th>Alamat Supplier</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    if(count($supplier) <= 0) {
                ?>
                    <tr>
                        <td colspan="4" class="text-center">Data Kosong</td>
                    </tr>
                <?php
                    }
                    foreach ($supplier as $key => $data) {
                ?>
                    <tr>
                        <td><?=$data['kode_supplier']?></td>
                        <td><?=$data['nama_supplier']?></td>
                        <td><?=$data['alamat_supplier']?></td>
                        <td>
                            <div class="btn-group">
                                <a href="<?=base_url()?>/supplier/edit/<?=$data['kode_supplier']?>" class="btn btn-sm btn-info">Ubah</a>
                                <a href="<?=base_url()?>/supplier/delete/<?=$data['kode_supplier']?>" onclick="return confirm('Apakah Anda yakin ingin menghapus <?= $data['nama_supplier']; ?> ?')" class="btn btn-sm btn-danger">Hapus</a>
                            </div>
                        </td>
                    </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
<?= $this->endSection() ?>