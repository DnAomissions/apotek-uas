<?= $this->extend('main_layout') ?>

<?= $this->section('setup') ?>
    <?php
        // Pengkondisian Isi dari Form Data
        $status = "create";
        $form_route = "/supplier/create";
        $breadcrumb = "Baru";
        $form_data = [
            'kode_supplier' => 'Otomatis Terisi',
            'nama_supplier' => '',
            'alamat_supplier' => '',
        ];

        // Jika Edit
        if(isset($supplier)){
            $status = "update";
            $form_route = "/supplier/update/".$supplier['kode_supplier'];
            $breadcrumb = $supplier['nama_supplier'];
            $form_data = [
                'kode_supplier' => $supplier['kode_supplier'],
                'nama_supplier' => $supplier['nama_supplier'],
                'alamat_supplier' => $supplier['alamat_supplier'],
            ];
        }
    ?>
<?= $this->endSection() ?>

<?= $this->section('navbar') ?>
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>/">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>/obat">Obat</a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="<?=base_url()?>/supplier">Supplier <span class="sr-only">(current)</span></a>
        </li>
    </ul>
<?= $this->endSection() ?>

<?= $this->section('breadcrumb') ?>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=base_url()?>/">Home</a></li>
        <li class="breadcrumb-item"><a href="<?=base_url()?>/supplier">Data Supplier</a></li>
        <li class="breadcrumb-item active" aria-current="page"><?=$breadcrumb?></li>
    </ol>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
    <div class="card">
        <div class="card-body">
            <form action="<?=base_url()?><?=$form_route?>" method="post">
                <div class="form-group">
                    <label for="kode_supplier">Kode Supplier</label>
                    <input type="text" name="kode_supplier" value="<?=$form_data['kode_supplier']?>" id="kode_supplier" class="form-control text-muted" readonly/>
                </div>
                <div class="form-group">
                    <label for="nama_supplier">Nama Supplier</label>
                    <input type="text" name="nama_supplier" value="<?=$form_data['nama_supplier']?>" id="nama_supplier" class="form-control" required/>
                </div>
                <div class="form-group">
                    <label for="alamat_supplier">Alamat Supplier</label>
                    <textarea name="alamat_supplier" id="alamat_supplier" rows="3" class="form-control"><?=$form_data['alamat_supplier']?></textarea>
                </div>
                <?php
                if($status == "create") {
                ?>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                <?php
                }
                ?>

                <?php
                if($status == "update") {
                ?>
                    <button type="submit" class="btn btn-info">Ubah</button>
                <?php
                }
                ?>
            </form>
        </div>
    </div>
<?= $this->endSection() ?>