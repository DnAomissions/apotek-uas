<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class ObatModel extends Model
{
    protected $table = "obat";
    protected $primaryKey = "kode_obat";

    public function getObat($id = false)
    {
        if($id === false){
            return $this->table($this->table)
                        ->get()
                        ->getResultArray();
        } else {
            return $this->table($this->table)
                        ->where($this->primaryKey, $id)
                        ->get()
                        ->getRowArray();
        }   
    }
 
    public function saveObat($data){
        $query = $this->db->table($this->table)->insert($data);
        return $query;
    }
 
    public function updateObat($data, $id)
    {
        $query = $this->db->table($this->table)->update($data, array($this->primaryKey => $id));
        return $query;
    }
 
    public function deleteObat($id)
    {
        $query = $this->db->table($this->table)->delete(array($this->primaryKey => $id));
        return $query;
    } 
}