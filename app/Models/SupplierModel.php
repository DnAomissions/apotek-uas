<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class SupplierModel extends Model
{
    protected $table = "supplier";
    protected $primaryKey = "kode_supplier";

    public function getSupplier($id = false)
    {
        if($id === false){
            return $this->table($this->table)
                        ->get()
                        ->getResultArray();
        } else {
            return $this->table($this->table)
                        ->where($this->primaryKey, $id)
                        ->get()
                        ->getRowArray();
        }   
    }
 
    public function saveSupplier($data){
        $query = $this->db->table($this->table)->insert($data);
        return $query;
    }
 
    public function updateSupplier($data, $id)
    {
        $query = $this->db->table($this->table)->update($data, array($this->primaryKey => $id));
        return $query;
    }
 
    public function deleteSupplier($id)
    {
        $query = $this->db->table($this->table)->delete(array($this->primaryKey => $id));
        return $query;
    } 
}